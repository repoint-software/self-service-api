# Self Service API (Node Express)
**Start Server -->** 
`node api.js`
> Default: localhost:3000 

## Database connection (PostgreSQL)
> Example: Demo Server Repoint (connection.js)


```
const {Client} = require('pg');

const client = new Client({
    host: "localhost",
    user: "repoint",
    port: 5432,
    password: "4repoint",
    database: "repoint"
});

module.exports = client;
```

## API Endpoints
### /article 
**GET** localhost:3000/article

**GET** localhost:3000/article/:plu

### /article-image
**GET** localhost:3000/article-image/:plu

PUT/DELETE localhost:3000/article-image/:plu



### /article-description
**GET** localhost:3000/article-description/:plu

PUT/DELETE localhost:3000/article-description/:plu


### /active

**GET** localhost:3000/active

DELETE localhost:3000/active

**GET** localhost:3000/active/:plu

DELETE localhost:3000/active/:plu

POST localhost:3000/active

### /config

**GET** localhost:3000/config

POST/PUT/DELETE localhost:3000/config


### /admin
**GET** localhost:3000:/admin

POST/PUT/DELETE localhost:3000/admin

### /areas
**GET** localhost:3000/areas

POST/DELETE localhost:3000/areas

GET/PUT/DELETE localhost:3000/areas/:id

**GET** localhost:3000/areas/:printer

### /areas-active
**GET** localhost:3000/areas-active/







